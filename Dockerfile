FROM amazon/aws-cli:2.11.19
RUN amazon-linux-extras install docker -y
RUN aws --version
RUN docker --version
ENTRYPOINT [""]

